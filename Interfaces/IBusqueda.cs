﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiSislgb.Interfaces
{
    public interface IBusqueda
    {
        public List<T> Buscar<T>(string tipoBusqueda);
    }
}
