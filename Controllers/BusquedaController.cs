﻿using apiSislgb.Interfaces;
using apiSislgb.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiSislgb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BusquedaController : ControllerBase
    {
        private readonly IBusqueda _busquedaService;

        public BusquedaController(IBusqueda busquedaService)
        {
            this._busquedaService = busquedaService;
        }

        [HttpGet("{tipoBusqueda}")]
        public ActionResult Busqueda(string tipoBusqueda)
        {

            if (tipoBusqueda == "C")
            {
                var listaBusqueda = this._busquedaService.Buscar<Compras>(tipoBusqueda);
                return Ok(listaBusqueda);
            } else if(tipoBusqueda == "F")
            {
                var listaBusqueda = this._busquedaService.Buscar<Facturas>(tipoBusqueda);
                return Ok(listaBusqueda);
            } else
            {
                var listaBusqueda = this._busquedaService.Buscar<int>(tipoBusqueda);
                return Ok(listaBusqueda);
            }
            
        }
    }
}
