﻿using apiSislgb.Interfaces;
using apiSislgb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiSislgb.Services
{
    public class BusquedaService : IBusqueda
    {
        public List<T> Buscar<T>(string tipoBusqueda)
        {

            List<int> listaNumeros = new List<int>();
            listaNumeros.Add(2);

            if (tipoBusqueda == "C")
            {
                List<Compras> listaCompras = new List<Compras>();
                listaCompras.Add(new Compras() { IdCompra = 1, NumeroDocumento = 23, Proveedor = "Carlos" });
                List<T> lista = listaCompras.Cast<T>().ToList();
                return lista;
            } else if(tipoBusqueda == "F")
            {
                List<Facturas> listaFacturas = new List<Facturas>();
                listaFacturas.Add(new Facturas() { IdFactura = 2, NumeroDocumento = 123456, Cliente = "Pedro" });
                List<T> lista = listaFacturas.Cast<T>().ToList();
                return lista;
            } else
            {
                List<T> lista = listaNumeros.Cast<T>().ToList();
                return lista;
            }
        }
    }
}
