﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiSislgb.Models
{
    public class Facturas
    {
        public int IdFactura { get; set; }

        public int NumeroDocumento { get; set; }

        public string Cliente { get; set; }
    }
}
