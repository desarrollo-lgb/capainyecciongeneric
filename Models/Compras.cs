﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiSislgb.Models
{
    public class Compras
    {
        public int IdCompra { get; set; }

        public int NumeroDocumento { get; set; }

        public string Proveedor { get; set; }
    }
}
