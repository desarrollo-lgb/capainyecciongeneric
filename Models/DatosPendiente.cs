﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiSislgb.Models
{
    public class DatosPendiente
    {
        public string CodProducto { get; set; }
        public string CodColor { get; set; }
        public int Pendiente { get; set; }

    }
}
